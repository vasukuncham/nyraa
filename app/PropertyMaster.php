<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class PropertyMaster extends Model
{
    protected $table = 'property_master';

    protected $primaryKey = 'product_code';


    public static function addProperty($inputs)
    {
    	$inst = new PropertyMaster;
    	$inst->product_code = $inputs['product_code'];
    	$inst->property_name = $inputs['property_type'];
    	$inst->property_value = $inputs['property_value'];
    	$inst->save();
    	return $inst;
    }
}
