<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class ProductMaster extends Model
{
    protected $table = 'product_master';

    public static function getTableColumnsForProducts() {

        $table1 = DB::getSchemaBuilder()->getColumnListing('product_master');
        $table2 = DB::getSchemaBuilder()->getColumnListing('property_master');
        return array_merge($table1, $table2);
    }

    public static function createFile($filename)
    {
    	try {
            Log::info("creating file: $filename");
            $fileobj = fopen($filename, 'w');
            return $fileobj;
        } catch (Exception $e) {
            Log::info("error occurred while creating file: $filename");
            exit(1);
        }
    }

    public static function writeIntoFile($fileobj, $data)
    {
    	try {
            $data = join("\n", $data);
            fwrite($fileobj, $data);
            fclose($fileobj);
        } catch (Exception $e) {
			Log::info("error occurred while writing into file");
            exit(1);
        }
    }

    public static function dumpToMysql($server, $user, $pwd, $db, $filename, $table) {
        try {
            $query =  "LOAD DATA LOCAL INFILE '$filename' REPLACE INTO TABLE $table CHARACTER SET latin1 FIELDS TERMINATED BY '#<>#'";
            
            $cmd = "mysql --init-command='SET SESSION FOREIGN_KEY_CHECKS=0;' -u $user -h $server -p$pwd $db --local-infile=1 -e \"$query\"";
            error_log("cmd: $cmd");
            error_log("dumping started");
            //$status = exec($cmd);
            exec("$cmd 2>&1", $output, $return_var);
            error_log("Status: $return_var");
            if($return_var == 1) {
                error_log("error occurred while dumping $filename into mysql");
                exit(1);
            }
        } catch (Exception $e) {
            error_log("error occurred while dumping into mysql");
            exit(1);
        }
    }

    public static function getProductById($code)
    {
        return ProductMaster::where('product_code', $code)->get();
    }

    public static function addProduct($inputs)
    {
        $inst = new ProductMaster;

        $inst->product_code = $inputs['code'];
        $inst->product_title = $inputs['title'];
        $inst->product_desc = $inputs['description'];
        $inst->mrp = $inputs['mrp'];
        $inst->retail_price = $inputs['retail_price'];
        $inst->discount = $inputs['discount'];
        $inst->status = $inputs['status'];
        $inst->category = $inputs['category'];
        $inst->size = $inputs['size'];
        $inst->color = $inputs['color'];
        $inst->product_style = $inputs['product_style'];
        $inst->product_type = $inputs['product_type'];
        $inst->save();

        return $inst;
    }
}
