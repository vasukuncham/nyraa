<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductMaster;
use App\ImageMaster;

class MainUIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = "HOME";
        $productData = ProductMaster::join('property_master', 'property_master.product_code', '=', 'product_master.product_code')
                    ->where('product_master.status', 'A')
                    ->get();
        if (count($productData) > 0) {
            foreach ($productData as $key => $value) {
                $productData[$key]['image_url'] = '';
                $imageList = ImageMaster::where('product_code', $value['product_code'])
                                ->where('priority', '1')
                                ->where('status', '1')
                                ->lists('image_url');
                if (count($imageList) > 0) {
                    $productData[$key]['image_url'] = $imageList[0];
                }
            }
        }
        $dataToView = array('page', 'productData');
        return view('pages.home', compact($dataToView));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $page = "CONTACT";
        $dataToView = array('page');
        return view('pages.contact', compact($dataToView));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
