<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::any('/', 'MainUIController@index');

Route::get('contact', 'MainUIController@contact');

Route::get('product_details', function () {
    return view('pages.product_details');
});
