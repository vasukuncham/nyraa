<?php

namespace App\Modules\AdminModule\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\ProductMaster;
use Input;
use Excel;
use Log;
use DB;
use Response;
use App\PropertyMaster;
use App\ImageMaster;

class AdminController extends Controller
{

    public $mysql_server, $mysql_user, $mysql_pwd, $mysql_db;


    public function __construct() {
        
        $this->mysql_server = 'localhost';
        $this->mysql_user = 'root';
        $this->mysql_pwd = 'root';
        $this->mysql_db = 'nyraa';
    }

    /**
     * Show Login Page
     *
     */
    public function showLogin()
    {
        return view('AdminModule::login');
    }

    /**
     * Validate User details for login
     *
     */
    public function checkLogin(Request $request)
    {
        $inputs = $request->all();

        if(isset($inputs['email']) && isset($inputs['password'])) {
            if (Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password']])) {

                $user_data = User::where('email','=',$inputs['email'])
                        ->where('user_role','=','admin')
                        ->where('status','=','A')
                        ->get();
                $user_data = $user_data[0];

                Session::put('userId', $user_data->id);
                Session::put('userName', $user_data->name);
                Session::put('userEmail', $user_data->email);
                Session::put('userMobile', $user_data->mobile);
                Session::put('userRole', $user_data->user_role);

                return redirect()->intended('admin/dashboard');
            } else {
                Session::flash('message', 'Given credentials are wrong. Please check and try again');
        
                return Redirect::to('/auth/login');
            }
        }
    }


    public function dashboard()
    {
        $page = 'AD_DASHBOARD';
        $dataToView = array('page');
        return view('AdminModule::dashboard', compact($dataToView));
    }


    public function products()
    {
        $page = 'AD_PRODUCTS';
        $productsData = ProductMaster::join('property_master', 'property_master.product_code', '=', 'product_master.product_code')->get();
        $dataToView = array('page', 'productsData');

        return view('AdminModule::products', compact($dataToView));
    }


    public function logout(){
        Session::flush();
        Session::flash('message', 'You have successfully logged out of the system.');
        
        return Redirect::to('/auth/login');
    }


    public function generateCommonSampleReport($tableFields, $table_name)
    {
        return Excel::create($table_name, function($excel) use ($tableFields){
            $excel->sheet('sheet 1', function($sheet) use ($tableFields){
                $sheet->fromArray($tableFields);
            });
        })->download('csv');
    }


    function deleteElement($element, &$array){
        $index = array_search($element, $array);
        if($index !== false){
            unset($array[$index]);
        }
    }
    

    public function downloadSampleFile()
    {
        $inputs = Input::all();

        if (isset($inputs['table']) && !empty($inputs['table'])) {
            
            switch ($inputs['table']) {
                
                case 'product_master';
                    $headers = ProductMaster::getTableColumnsForProducts();
                    $unsetElements = array('created_at', 'created_at', 'updated_at', 'updated_at', 'id', 'product_code');

                    foreach ($unsetElements as $key => $value) {
                        $this->deleteElement($value, $headers);  
                    }
                    return $this->generateCommonSampleReport($headers, $inputs['table']);
                    break;

                default:
                    return "<h3>table was not found Please close this tab and try again later.</h3>";
                    break;
            }
        } else {
            return "<h3>table was not found Please close this tab and try again later.</h3>";
        }
    }


    public function validatePoItemDetailTable($rec)
    {
        $errorArray = array();
            
        if (empty($rec['product_code'])) {
            array_push($errorArray, 'Product Code Should bot be blank');
        }

        if (empty($rec['product_title'])) {
            array_push($errorArray, 'Product Title Should bot be blank');
        }

        if (empty($rec['product_desc'])) {
            array_push($errorArray, 'Product Description Should bot be blank');
        }

        if (empty($rec['mrp']) && preg_match("/[a-z]/i", $rec['mrp'])) {
            array_push($errorArray, 'MRP Should bot be blank or should contain only numbers');
        }

        if (empty($rec['retail_price']) && preg_match("/[a-z]/i", $rec['retail_price'])) {
            array_push($errorArray, 'Retail Price Should bot be blank or should contain only numbers');
        }

        if (empty($rec['product_type'])) {
            array_push($errorArray, 'Product Type Should bot be blank');
        }

        if (empty($rec['product_style'])) {
            array_push($errorArray, 'Product Style Should bot be blank');
        }

        if (empty($rec['discount']) && preg_match("/[a-z]/i", $rec['discount'])) {
            array_push($errorArray, 'Discount Should bot be blank or should contain only numbers');
        }

        if (!in_array($rec['status'], ['A', 'N'])) {
            array_push($errorArray, 'Status Should bot be blank or Should be either A or N.');
        }

        if (empty($rec['category'])) {
            array_push($errorArray, 'category Should bot be blank');
        }

        if (empty($rec['size'])) {
            array_push($errorArray, 'Size Should bot be blank');
        }

        if (empty($rec['color'])) {
            array_push($errorArray, 'Color Should bot be blank');
        }

        if (empty($rec['property_name'])) {
            array_push($errorArray, 'Property Name Should bot be blank');
        }

        if (empty($rec['property_value'])) {
            array_push($errorArray, 'Property value Should bot be blank');
        }

        return $errorArray;
    }


    public function downloadErrorReport($sheet_name, $docsWithErrors, $csvHeaders)
    {
        $myFile = Excel::create($sheet_name, function($excel) use ($docsWithErrors, $csvHeaders){
                $excel->sheet('sheet 1', function($sheet) use ($docsWithErrors, $csvHeaders){
                    $sheet->row(1, $csvHeaders);
                    $sheet->fromArray($docsWithErrors, null, 'A2', false, false);
                });
            });
        return $myFile = $myFile->string('csv');
    }


    public function uploadProductsData()
    {
        $inputs = Input::all();
        $csvHeaders = [];
        $successFlashMeg = $errorFlashMsg = array();
        $file = Input::file('file');
        $WithErrors = $productWithErrorFree = $propertyWithErrorFree = array();
        $content = Excel::load($file)->get();

        if (count($content) > 0) {
            foreach ($content as $key => $value) {

                if ($key == 0) {
                   $csvHeaders = array_keys(json_decode(json_encode($value), true));
                }
                $errors = $this->validatePoItemDetailTable($value);
                
                if (count($errors) > 0) {
                    $errorRec = [
                        $value['product_code'], $value['product_title'], $value['product_desc'], 
                        $value['mrp'], $value['retail_price'], $value['product_type'], 
                        $value['discount'], $value['status'], $value['category'], $value['size'], 
                        $value['color'], $value['product_style'], $value['property_name'], 
                        $value['property_value'], implode(', ', $errors)
                    ];

                    array_push($WithErrors, array_values($errorRec));
                } else {
                    $productRec = [
                        $value['product_code'], $value['product_title'], $value['product_desc'], 
                        $value['mrp'], $value['retail_price'], $value['product_type'], 
                        $value['discount'], $value['status'], $value['category'], $value['size'], 
                        $value['color'], $value['product_style'], date('Y-m-d H:i:s'), date('Y-m-d H:i:s')
                    ];

                    $propertyRec = [
                        $value['product_code'], $value['property_name'], $value['property_value'], date('Y-m-d H:i:s'), date('Y-m-d H:i:s')
                    ];

                    array_push($productWithErrorFree, implode("#<>#", $productRec));

                    array_push($propertyWithErrorFree, implode("#<>#", $propertyRec));
                }
            }

            ### Creating csv file with error message and sending to user ###
            if (count($WithErrors) > 0) {
                Session::flash('errorMsg', count($WithErrors)." Record(s) Has failed Validation. Please check and re-upload again");
                
                $errorFile = $this->downloadErrorReport("ProductMaster_errors", $WithErrors, $csvHeaders);

                $response =  array(
                   'status'=> 'error',
                   'name' => "ProductMasterErrorList.csv",
                   'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($errorFile)
                );
                return response()->json($response);

            }

            ### Creating file to upload into DB ###
            if (count($productWithErrorFree) > 0) {
                Log::info("creating a file");
                $filePath = public_path()."/productmaster.txt";
                $fileobj = ProductMaster::createFile($filePath);

                Log::info("writing into file: ".$filePath);
                ProductMaster::writeIntoFile($fileobj, $productWithErrorFree);

                Log::info("Dumping File started");
                ProductMaster::dumpToMysql($this->mysql_server, $this->mysql_user, $this->mysql_pwd, $this->mysql_db, $filePath, 'product_master');
                Log::info("Loaded data into table");

                Session::flash('successMsg', count($productWithErrorFree)." Record(s) Has been Uploaded Into Database");
            }

            ### Creating file to upload into DB ###
            if (count($propertyWithErrorFree) > 0) {
                Log::info("creating a file");
                $filePath = public_path()."/propertymaster.txt";
                $fileobj = ProductMaster::createFile($filePath);

                Log::info("writing into file: ".$filePath);
                ProductMaster::writeIntoFile($fileobj, $propertyWithErrorFree);

                Log::info("Dumping File started");
                ProductMaster::dumpToMysql($this->mysql_server, $this->mysql_user, $this->mysql_pwd, $this->mysql_db, $filePath, 'property_master');
                Log::info("Loaded data into table");

                Session::flash('successMsg', count($propertyWithErrorFree)." Record(s) Has been Uploaded Into Database");
            }

            return response::json(array('status'=> "success"));
        } else {
            return response::json(array('status'=> "failure", "message"=> "No data found in file to parse"));
        }
    }


    public function productImages()
    {
        $page = 'AD_PRODUCT_IMAGES';
        $data = ProductMaster::select('product_code', 'product_title')
                    ->where('status', 'A')
                    ->get();
        $dataToView = array('page', 'data');
        return view('AdminModule::productImages', compact($dataToView));
    }

    public function getProductById()
    {
        $code = Input::get('code');
        $data = ProductMaster::getProductById($code);
        $images = ImageMaster::where('product_code', $code)->get();

        if ($data) {
            return Response::json(array('status'=> 'success', 'data'=> $data, 'images'=> $images));
        }else{
            return Response::json(array('status'=> 'success', 'data'=> ''));
        }
    }

    public function uploadProductImages()
    {
        $inputs = Input::all();
        $file = Input::file('file');

        if ($file) {
            $extension = Input::file('file')->getMimeType();
            $final =  explode('/', $extension );

            if ($final[1] == "png" || $final[1] == "jpg" || $final[1] == "jpeg") {
                $destinationPath = 'assets/product_images';
                $originalName = $file->getClientOriginalName();
                $filename = $inputs['product_code']."-".$originalName;
                Input::file('file')->move($destinationPath, $filename);
                $inst = new ImageMaster;
                $inst->product_code = $inputs['product_code'];
                $inst->image_url = $filename;
                $inst->priority = explode('.', $originalName)[0];
                $inst->status = '1';
                $inst->save();
            }
        }
        return Response::json(array('status'=> 'success'));
    }

    public function updateProductsData(Request $request)
    {
        $inputs = $request->all();
        DB::beginTransaction();
        try {
            $arr = array(
                'product_title'=> $inputs['title'],
                'product_desc'=> $inputs['description'],
                'mrp'=> $inputs['mrp'],
                'retail_price'=> $inputs['retail_price'],
                'discount'=> $inputs['discount'],
                'status'=> $inputs['status'],
                'category'=> $inputs['category'],
                'size'=> $inputs['size'],
                'color'=> $inputs['color'],
                'product_style'=> $inputs['product_style']
            );

            ProductMaster::where('product_code', $inputs['code'])
                        ->update($arr);

            $propertyMaster = PropertyMaster::where('product_code', $inputs['code'])
                            ->update(['property_name'=> $inputs['property_type'], 'property_value'=> $inputs['property_value']]);
            DB::commit();
            return Response::json([
                        'status' => true,
                        'message' => 'updated successfully',
                    ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json([
                        'status' => false,
                        'message' => $e->getMessage(),
                    ], 400);
        }
    }


    public function addProduct(Request $request)
    {
        $inputs = $request->all();
        $pMaster = ProductMaster::addProduct($inputs);

        if ($pMaster) {
            $propMaster = PropertyMaster::addProperty($inputs);
        }
        $productsData = ProductMaster::join('property_master', 'property_master.product_code', '=', 'product_master.product_code')->get();
        return Response::json(array('status'=> 'success', 'data'=> $productsData));
    }


    public function deleteProduct()
    {
        $inputs = Input::all();
        $delete = ProductMaster::where('product_code', $inputs['code'])
                    ->update(['status'=> 'N']);

        return Response::json(array('status'=> 'success'));
    }
}
