<?php

$namespace = 'App\Modules\AdminModule\Controllers';
$middleware = 'App\Modules\AdminModule\Middleware';

/*
|--------------------------------------------------------------------------
| Core Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an Core Module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'auth', 'namespace' => $namespace], function() {
	Route::any('/login', 'AdminController@showLogin');
	Route::any('/checkLogin', 'AdminController@checkLogin');
	Route::any('/logout', 'AdminController@logout');
});

Route::group(['prefix' => 'admin', 
			  'namespace' => $namespace, 
			  'middleware'=> $middleware."\AdminMiddleware"], function() {
	Route::any('/dashboard', 'AdminController@dashboard');
	Route::any('/products', 'AdminController@products');
	Route::any('/productImages', 'AdminController@productImages');
	Route::any('/downloadSampleFile', 'AdminController@downloadSampleFile');
	Route::any('/uploadProductsData', 'AdminController@uploadProductsData');
	Route::get('/getProductById', 'AdminController@getProductById');
	Route::any('/uploadProductImages', 'AdminController@uploadProductImages');
	Route::post('/addProduct', 'AdminController@addProduct');
	Route::post('/updateProduct', 'AdminController@updateProductsData');
	Route::post('/deleteProduct', 'AdminController@deleteProduct');
});

