<?php

namespace App\Modules\AdminModule\Middleware;

use Closure;
use Session;
use Auth;
use Illuminate\Support\Facades\Redirect;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

      if(Auth::guest()){

        return redirect()->guest('/auth/logout');     
      }

      return $next($request);
    }
}
