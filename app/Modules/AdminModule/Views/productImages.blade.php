@extends('layout.admin.htmlbase')

@section('CSS')
	<!-- Dropzone Css -->
    <link href="{{url()}}/assets/admin/dropzone/dropzone.css" rel="stylesheet">
    <!-- JQuery DataTable Css -->
    <link href="{{url()}}/assets/admin/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Light Gallery Plugin Css -->
    <link href="{{url()}}/assets/admin/light-gallery/css/lightgallery.css" rel="stylesheet">

    <style type="text/css">
    	.btn > .material-icons {
    		color: #fff;
    	}

    	.btn.dropdown-toggle {
    		border: 1px solid #f2f2f2;
    		padding: 15px;
    		background-color: #f2f2f2 !important;
    	}
    	ul li {
    		border: none !important;
    	}
    </style>
@stop

@section('pageBackLink')
	<a href="{{url()}}/admin/dashboard" style="position: absolute; margin-top: -5px;">
		<i class="material-icons" style="color: #fff; font-size: 35px">keyboard_backspace</i>
	</a>
@stop

@section('currentPageName')
	Product Images
@stop

@section('Content')
	<div class="PAGE_DASHBOARD">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
                        <h2>
                            Upload Images for Products
                        </h2>
                    </div>
	                <div class="body">
	                	<div class="row m-b-20">
	                		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
	                			<select class="form-control show-tick product_dropdown" data-live-search="true">
	                				@if(count($data) > 0)
	                				<option value="">Please Select Product</option>
	                				@foreach($data as $k => $val)
	                					<option value="{{$val['product_code']}}">{{$val['product_title']}}</option>
	                				@endforeach
	                				@endif
                                </select>
	                		</div>
	                	</div>
	                	<!-- Bulk Upload form  start-->
	                	<div class="row">
	                		<div class="col-md-4 col-sm-12 col-xs-12">
	                			<ul class="list-group">
                                	<li class="list-group-item text-left">
                                		<strong>Name:</strong>
                                		<div class="title"></div>
                                	</li>
                                	<li class="list-group-item text-left">
                                		<strong>Description:</strong>
                                		<div class="desc"></div>
                                	</li>
                                	<li class="list-group-item text-left">
                                		<strong>Style / Category:</strong>
                                		<div class="style-cat"></div>
                                	</li>
                                	<li class="list-group-item text-left">
                                		<strong>Size / Color:</strong>
                                		<div class="size-color"></div>
                                	</li>
                            	</ul>
	                		</div>
	                		<div class="col-md-8 col-sm-12 col-xs-12">
	                			<form action="{{url()}}/admin/uploadProductImages" id="formFileUpload" class="dropzone m-b-15" method="post" enctype="multipart/form-data">
	                				<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token(); ?>">
	                                <div class="dz-message">
	                                    <div class="drag-icon-cph">
	                                        <i class="material-icons">touch_app</i>
	                                    </div>
	                                    <h3>Drop Images here or click to upload.</h3>
	                                </div>
	                                	<input type="hidden" name="product_code" class="product_code">
	                                <div class="fallback">
	                                    <input name="file" class="fileUpload" type="file" multiple />
	                                </div>
	                            </form>
	                		</div>
	                	</div>
	                	<!-- Bulk Upload form  end-->
	                </div>
	            </div>

	            <div class="card">
					<div class="header">
                        <h2>
                            Product Images
                        </h2>
                    </div>
	                <div class="body">
	                	<div id="aniimated-thumbnials" class="list-unstyled row clearfix" align="center">
	                        No Images Found to display
	                    </div>
	                </div>
	            </div>
			</div>
		</div>


	</div>
@stop

@section('JS')
	<!-- Dropzone Plugin Js -->
    <script src="{{url()}}/assets/admin/dropzone/dropzone.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{url()}}/assets/admin/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/buttons.html5.min.js"></script>

    <script src="{{url()}}/assets/admin/js/pages/tables/jquery-datatable.js"></script>
    <!-- Light Gallery Plugin Js -->
    <script src="{{url()}}/assets/admin/js/pages/medias/image-gallery.js"></script>
    <script src="{{url()}}/assets/admin/light-gallery/js/lightgallery-all.js"></script>
    <script src="{{url()}}/assets/admin/js/pages/ui/notifications.js"></script>
    <script src="{{url()}}/assets/admin/custom-js/image_master.js"></script>
@stop

