@extends('layout.admin.htmlbase')

@section('CSS')
	<!-- Dropzone Css -->
    <link href="{{url()}}/assets/admin/dropzone/dropzone.css" rel="stylesheet">
    <!-- JQuery DataTable Css -->
    <link href="{{url()}}/assets/admin/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <style type="text/css">
    	.btn > .material-icons {
    		color: #fff;
    	}
    </style>
@stop

@section('pageBackLink')
	<a href="{{url()}}/admin/dashboard" style="position: absolute; margin-top: -5px;">
		<i class="material-icons" style="color: #fff; font-size: 35px">keyboard_backspace</i>
	</a>
@stop

@section('currentPageName')
	Products
@stop

@section('Content')
	<div class="PAGE_DASHBOARD">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
                        <h2>
                            Upload Bulk Products
                        </h2>
                    </div>
	                <div class="body">
	                	<div class="row">
	                		<div class="col-md-6 col-md-offset-3">
	                			<form action="{{url()}}/admin/uploadProductsData" id="frmFileUpload" class="dropzone m-b-15" method="post" enctype="multipart/form-data">
	                				<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token(); ?>">
	                                <div class="dz-message">
	                                    <div class="drag-icon-cph">
	                                        <i class="material-icons">touch_app</i>
	                                    </div>
	                                    <h3>Drop files here or click to upload.</h3>
	                                </div>
	                                <div class="fallback">
	                                    <input name="file" type="file" />
	                                </div>
	                            </form>
	                            <a href="{{url()}}/admin/downloadSampleFile?table=product_master" target="_blank">
	                            	<small>Download product table csv file with required headers</small>
	                            </a>
	                		</div>
	                	</div>
	                </div>
	            </div>

                <div class="card">
                    <div class="header">
                        <h2>
                            Add Products
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row m-t-20 m-r-20 m-l-20">
                            <form class="addProduct" id="addProduct" method="POST" action="{{url()}}/admin/addProduct">
                                {!! csrf_field() !!}
                                <div class="row clearfix m-b-20">
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control code" name="product_code" required>
                                                <input type="hidden" class="form-control code" name="code" >
                                                <label class="form-label">Code</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control title"  name="title" required>
                                                <label class="form-label">Title</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="mrp" type="number" step="0.01" class="form-control mrp" required>
                                                <label class="form-label">MRP</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="product_type" type="text" class="form-control product_type" required>
                                                <label class="form-label">Product Type</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix m-b-20">
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="retail_price" type="number" step="0.01" class="form-control retail_price">
                                                <label class="form-label">Retail Price</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="discount" type="number" step="0.01" class="form-control discount" required>
                                                <label class="form-label">Discount</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <select class="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="A">Actvie</option>
                                                <option value="N">In-Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="category" class="form-control category" required>
                                                <label class="form-label">Category</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix m-b-20">
                                        <div class="col-sm-3">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" name="size" class="form-control size" required>
                                                    <label class="form-label">Size</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" name="color" class="form-control color"  required>
                                                    <label class="form-label">Color</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" name="product_style" class="form-control product_style" required>
                                                    <label class="form-label">Product Style</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" name="property_type" class="form-control property_type" required>
                                                    <label class="form-label">Property Type</label>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="row clearfix m-b-20">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <textarea name="property_value" class="form-control property_value" height="200" required></textarea>
                                                <label class="form-label">Property Value</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <textarea name="description" class="form-control description" height="200" required></textarea>
                                                <label class="form-label">Description</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info btn-link waves-effect" style="color: #fff">Add Product</button>
                            </form>
                        </div>
                    </div>
                </div>

	            <div class="card">
					<div class="header">
                        <h2>
                            Product List
                        </h2>
                    </div>
	                <div class="body">
	                	<!-- View All uploaded products start-->
	                	<div class="table-responsive">
		                	<table class="table table-bordered table-striped table-hover dataTable js-exportable">
	                            <thead>
	                                <tr>
	                                    <th>Code</th>
	                                    <th>Title</th>
	                                    <th>Description</th>
	                                    <th>MRP</th>
	                                    <th>Retail Price</th>
	                                    <!-- <th>Product Type</th> -->
	                                    <th>Discount</th>
	                                    <th>Status</th>
	                                    <th>Category</th>
	                                    <th>Size</th>
	                                    <th>Color</th>
	                                    <th>Product Style</th>
	                                    <!-- <th>Property Type</th>
	                                    <th>Property Value</th> -->
	                                    <!-- <th>Created At</th> -->
	                                    <th>Actions</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($productsData as $key => $product)
		                                <tr>
		                                    <td>{{$product['product_code']}}</td>
		                                    <td>{{$product['product_title']}}</td>
		                                    <td>{{$product['product_desc']}}</td>
		                                    <td>{{$product['mrp']}}</td>
		                                    <td>{{$product['retail_price']}}</td>
		                                    <!-- <td>{{-- $product['product_type'] --}}</td> -->
		                                    <td>{{$product['discount']}}</td>
		                                    <td>{{$product['status']}}</td>
		                                    <td>{{$product['category']}}</td>
		                                    <td>{{$product['size']}}</td>
		                                    <td>{{$product['color']}}</td>
		                                    <td>{{$product['product_style']}}</td>
		                                    <!-- <td>{{-- $product['property_name'] --}}</td>
		                                    <td>{{-- $product['property_value'] --}}</td> -->
		                                    <!-- <td>{{-- $product['created_at'] --}}</td> -->
		                                    <td>
		                                    	<a href="javascript:void(0)" class="ModifyProduct" title="Modify this Product" data-code="{{$product['product_code']}}" data-product_title="{{$product['product_title']}}" 
                                                data-product_desc="{{$product['product_desc']}}" data-mrp="{{$product['mrp']}}" 
                                                data-retail_price="{{$product['retail_price']}}" data-discount="{{$product['discount']}}" data-status="{{$product['status']}}" data-category="{{$product['category']}}" data-size="{{$product['size']}}" 
                                                data-color="{{$product['color']}}" data-product_style="{{$product['product_style']}}" 
                                                data-property_type="{{$product['property_name']}}"
                                                data-property_value="{{$product['property_value']}}"
                                                >
					                               Modify
					                            </a>
                                                |
					                            <a href="javascript:void(0)" class="deleteProduct" title="Delete This Product" data-code="{{$product['product_code']}}">
					                                Delete
					                            </a>
		                                    </td>
		                                </tr>
		                            @endforeach
	                            </tbody>
	                        </table>
	                    </div>
	                	<!-- View All uploaded products end-->
	                </div>
	            </div>
			</div>
		</div>

		
		<!-- Modify User -->
        <div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">Delete Product</h4>
                    </div>
                    <div class="modal-body">
                        <h5>Are You Sure! You want to delete Product ?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect deleteProductConfirm" data-code="" data-csrf="{{ csrf_token() }}">DELETE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>

		<!-- Large Size -->
        <div class="modal fade" id="ModifyProductModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header m-b-15">
                        <h4 class="modal-title" id="largeModalLabel">Update Product Details</h4>
                    </div>
                    <div class="modal-body">
                        <form class="updateProduct" id="updateProduct" method="POST" action="{{url()}}/admin/updateProduct">
                            {!! csrf_field() !!}
                            <div class="row clearfix m-b-20">
                                <div class="col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control code" name="product_code" required disabled>
                                            <input type="hidden" class="form-control code" name="code" >
                                            <label class="form-label">Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control title"  name="title" required>
                                            <label class="form-label">Title</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input name="mrp" type="number" step="0.01" class="form-control mrp" required>
                                            <label class="form-label">MRP</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="product_type" type="text" class="form-control product_type" required>
                                            <label class="form-label">Product Type</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix m-b-20">
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input name="retail_price" type="number" step="0.01" class="form-control retail_price">
                                            <label class="form-label">Retail Price</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input name="discount" type="number" step="0.01" class="form-control discount" required>
                                            <label class="form-label">Discount</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <!-- <input type="text" name="status" class="form-control status" required> -->
                                            <select class="status" name="status">
                                                <option value="A">Actvie</option>
                                                <option value="N">In-Active</option>
                                            </select>
                                           
                                            <label class="form-label">Status</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" name="category" class="form-control category" required>
                                            <label class="form-label">Category</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix m-b-20">
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <input type="text" name="size" class="form-control size" required>
                                                <label class="form-label">Size</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <input type="text" name="color" class="form-control color"  required>
                                                <label class="form-label">Color</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <input type="text" name="product_style" class="form-control product_style" required>
                                                <label class="form-label">Product Style</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <input type="text" name="property_type" class="form-control property_type" required>
                                                <label class="form-label">Property Type</label>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row clearfix m-b-20">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <textarea name="property_value" class="form-control property_value" height="100" required></textarea>
                                            <label class="form-label">Property Value</label>
                                        </div>
                                    </div>
                                </div>
                            	<div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <textarea name="description" class="form-control description" height="100" required></textarea>
                                            <label class="form-label">Description</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>


	</div>
@stop

@section('JS')
	<!-- Dropzone Plugin Js -->
    <script src="{{url()}}/assets/admin/dropzone/dropzone.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{url()}}/assets/admin/jquery-datatable/jquery.dataTables.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="{{url()}}/assets/admin/jquery-datatable/extensions/export/buttons.html5.min.js"></script>

    <script src="{{url()}}/assets/admin/js/pages/tables/jquery-datatable.js"></script>
    <script src="{{url()}}/assets/admin/js/pages/ui/notifications.js"></script>
    <script src="{{url()}}/assets/admin/custom-js/product_master.js"></script>
@stop

