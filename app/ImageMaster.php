<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class ImageMaster extends Model
{
    protected $table = 'image_master';

    protected $primaryKey = 'product_code';

    public $timestamps = false;

}
