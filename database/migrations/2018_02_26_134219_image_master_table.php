<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImageMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_master', function (Blueprint $table) {
            $table->string('product_code', 50);
            $table->string('image_url');
            $table->integer('priority');
            $table->boolean('status')->default(1);

            $table->unique( array('product_code', 'image_url', 'priority') );

            $table->foreign('product_code')
                  ->references('product_code')
                  ->on('product_master')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_master');
    }
}
