<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PropertyMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_master', function (Blueprint $table) {
            $table->string('product_code', 50)->primary();
            $table->string('property_name');
            $table->longText('property_value');
            $table->timestamps();

            $table->foreign('product_code')
                  ->references('product_code')
                  ->on('product_master')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_master');
    }
}
