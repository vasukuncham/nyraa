<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_master', function (Blueprint $table) {
            $table->string('product_code', 50)->primary();
            $table->string('product_title');
            $table->longText('product_desc');
            $table->float('mrp');
            $table->float('retail_price');
            $table->string('product_type');
            $table->float('discount');
            $table->enum('status', array("A","N"));
            $table->string('category');
            $table->string('size');
            $table->string('color');
            $table->string('product_style');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_master');
    }
}
