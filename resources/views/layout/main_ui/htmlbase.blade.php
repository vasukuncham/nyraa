<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('TitleName')</title>

    <!-- Favicon-->
    @section('Favicon')
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{url()}}/assets/main_ui/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{url()}}/assets/main_ui/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{url()}}/assets/main_ui/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="{{url()}}/assets/main_ui/images/ico/apple-touch-icon-57-precomposed.png">
    @show

    @section('FontAwesome')
        
    @show

    <link href="{{url()}}/assets/main_ui/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url()}}/assets/main_ui/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url()}}/assets/main_ui/css/prettyPhoto.css" rel="stylesheet">
    <link href="{{url()}}/assets/main_ui/css/price-range.css" rel="stylesheet">
    <link href="{{url()}}/assets/main_ui/css/animate.css" rel="stylesheet">
    <link href="{{url()}}/assets/main_ui/css/main.css" rel="stylesheet">
    <link href="{{url()}}/assets/main_ui/css/responsive.css" rel="stylesheet">

    @yield('CSS')

</head>

<body>
    <!-- Top Bar -->
    @include('layout.main_ui.header')
    <!-- Top Bar End-->

    @yield('slider')

    <section>
        <div class="container">
            @yield('Content')
        </div>
    </section>

    <!-- Top Slider -->
    @include('layout.main_ui.footer')
    <!-- Top Slider End-->

    <script src="{{url()}}/assets/main_ui/js/jquery.js"></script>
    <script src="{{url()}}/assets/main_ui/js/bootstrap.min.js"></script>
    <script src="{{url()}}/assets/main_ui/js/jquery.scrollUp.min.js"></script>
    <script src="{{url()}}/assets/main_ui/js/price-range.js"></script>
    <script src="{{url()}}/assets/main_ui/js/jquery.prettyPhoto.js"></script>
    <script src="{{url()}}/assets/main_ui/js/main.js"></script>
    <script type="text/javascript">
        var apiUrl = "{{url()}}";
    </script>

    @yield('JS')

</body>

</html>