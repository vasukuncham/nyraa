<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar closed">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{url()}}/assets/admin/images/user.png" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{Session::get('userName')}}
            </div>
            <div class="email">{{Session::get('userEmail')}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color: #fff;">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="{{url()}}/admin/profile"><i class="material-icons">person</i>Profile</a></li>
                    <li role="seperator" class="divider"></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="HOME {{ $page == 'AD_DASHBOARD' ? 'active' : ''}}">
                <a href="{{url()}}/admin/dashboard">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li class="PRODUCTS {{ $page == 'AD_PRODUCTS' ? 'active' : ''}}">
                <a href="{{url()}}/admin/products">
                    <i class="material-icons">assignment</i>
                    <span>Product List</span>
                </a>
            </li>
            <li class="PRODUCT_IMAGES {{ $page == 'AD_PRODUCT_IMAGES' ? 'active' : ''}}">
                <a href="{{url()}}/admin/productImages">
                    <i class="material-icons">perm_media</i>
                    <span>Product Images</span>
                </a>
            </li>
            <li class="LOGOUT">
                <a href="{{url()}}/auth/logout">
                    <i class="material-icons">input</i>
                    <span>Logout</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2018 - 2019 <a href="javascript:void(0);">Nyraa - Admin Portal</a>.
        </div>
        <div class="version">
            <b>Version: </b> 0.1
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar
