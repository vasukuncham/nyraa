<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    @section('Title')
    <title>Admin Dashboard</title>
    @show

    <!-- Favicon-->
    @section('Favicon')
    <!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->
     <link rel="shortcut icon" type="image/x-icon" href="{{url()}}/assets/favicon/favicon.ico" />
    @show

    @section('FontAwesome')
    <link rel="stylesheet" href="{{url()}}/assets/admin/font-awesome/css/font-awesome.min.css">
    @show


    <!-- Google Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> -->

    <!-- Bootstrap Core Css -->
    <link href="{{url()}}/assets/admin/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{url()}}/assets/admin/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{url()}}/assets/admin/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{url()}}/assets/admin/css/style.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{url()}}/assets/admin/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{url()}}/assets/admin/css/themes/all-themes.css" rel="stylesheet" />

    <!-- FOnts -->
    <link href="{{url()}}/assets/admin/css/googleFonts.css" rel="stylesheet">

    <!-- FOnts -->
    <link href="{{url()}}/assets/admin/css/googleIcons.css" rel="stylesheet">

    <link href="{{url()}}/assets/admin/management.css" rel="stylesheet" />

    @yield('CSS')

</head>

<body class="theme-red">
    <!-- Page Loader -->
     <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->

    <!-- Top Bar -->
    @include('layout.admin.header')
    <!-- Top Bar End-->

    <!-- Left & Right Sidebars -->
    <section>
        @include('layout.admin.left_sidebar')
    </section>
    <!-- Left & Right Sidebars end-->
    <section>
        <div class="row page_title">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="left">
                  @yield('pageBackLink')
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" align="center">
                  @yield('currentPageName')
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="center">
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            @yield('Content')
        </div>
    </section>



    <!-- Jquery Core Js -->
    <script src="{{url()}}/assets/admin/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{url()}}/assets/admin/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="{{url()}}/assets/admin/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{url()}}/assets/admin/jquery-slimscroll/jquery.slimscroll.js"></script> 

    <!-- Waves Effect Plugin Js -->
    <script src="{{url()}}/assets/admin/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{url()}}/assets/admin/jquery-countto/jquery.countTo.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{url()}}/assets/admin/jquery-sparkline/jquery.sparkline.js"></script>

    <script src="{{url()}}/assets/admin/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Custom Js -->
    <script src="{{url()}}/assets/admin/js/admin.js"></script>

    <script type="text/javascript">
        var apiUrl = "{{url()}}";
        ;(function(){
            var $body = $('body');
            var $overlay = $('.overlay');

            $('.navbar .navbar-header .nav-bar-icon, .overlay').on('click', function (e) {
                e.preventDefault();
                if ($('.sidebar').hasClass('closed')) {
                    $('.sidebar').css({'margin-left': '0px'});
                    $('.sidebar').removeClass('closed')
                    $('.overlay').show();
                    $('.navbar .navbar-header .nav-bar-icon').find('.material-icons').text('close');
                }else{
                    $('.sidebar').css({'margin-left': '-300px'});
                    $('.sidebar').addClass('closed')
                    $('.overlay').hide();
                    $('.navbar .navbar-header .nav-bar-icon').find('.material-icons').text('reorder');
                }
            });
        }());
    </script>

    @yield('JS')

</body>

</html>