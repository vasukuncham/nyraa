;(function(argument) {
	
	$(document).on('change', 'select.product_dropdown', function() {
		var code = $(this).val();
		$('#formFileUpload').find('.product_code').val(code);
		if (code != '') {
			$.ajax({
				url: apiUrl+'/admin/getProductById',
				type: 'GET',
				data: {'code': code},
				success: function(resp) {
				 	if (resp.status == 'success') {
				 		var images = resp.images;
				 		resp = resp.data[0];
				 		$('.title').text(resp.product_title);
				 		$('.desc').text(resp.product_desc);
				 		$('.style-cat').text(resp.product_style +" / "+ resp.category);
				 		$('.size-color').text(resp.size +" / "+ resp.color);
				 		showNotification('bg-black', 'Fetched Product details');

				 		var imageContent = '';
				 		for (var i = 0; i < images.length; i++) {
				 			var url = apiUrl+'/assets/product_images/'+images[i]['image_url'];
				 			imageContent += '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">'+
	                            '<a href="#" data-sub-html="'+resp.product_title+'">'+
	                                '<img class="img-responsive thumbnail" src="'+url+'">'+
	                            '</a>'+
	                        '</div>';
				 		}
				 		$('#aniimated-thumbnials').html(imageContent);
				 	}else {
				 		showNotification('bg-success', 'Something went wrong');
				 	}
				}
		    });
		}
	});
}());