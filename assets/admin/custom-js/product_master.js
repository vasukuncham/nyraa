;(function(argument) {
  
	$(document).on('click', '.ModifyProduct', function function_name(argument) {

	    $('.code').val($(this).attr('data-code'));
	    $('.product_code').val($(this).attr('data-code'));
	    $('.title').val($(this).attr('data-product_title'));
	    $('.description').val($(this).attr('data-product_desc'));
	    $('.mrp').val($(this).attr('data-mrp'));
	    $('.retail_price').val($(this).attr('data-retail_price'));
	    $('.discount').val($(this).attr('data-discount'));
	    $('.category').val($(this).attr('data-category'));
	    $('.size').val($(this).attr('data-size'));
	    $('.color').val($(this).attr('data-color'));
	    $('.product_style').val($(this).attr('data-product_style'));
	    $('.property_type').val($(this).attr('data-property_type'));
	    $('.property_value').val($(this).attr('data-property_value'));
	    var status_data = $(this).attr('data-status');
	    $('select[name="status"]').val(status_data);
	    $('#ModifyProductModal').modal('show');
	});

	$('.updateProduct').submit(function(e) {
	  	
	  	e.preventDefault();
	  	
	  	var formData = JSON.parse(JSON.stringify($('.updateProduct').serializeArray()));
	  	$.ajax({
		    url: apiUrl+'/admin/updateProduct',
		    type: 'POST',
		    data: formData,
		    success: function(resp) {
		      	if (resp.status) {
		      		showNotification('bg-black', 'Details has been updated successfully');
		        	setTimeout(function() {
		        		window.location.reload(1);
		        	}, 4000);
		      	} else {
			 		showNotification('bg-danger', 'Something went wrong!');
			 	}
		    }
		});
  	});

  	$('.addProduct').submit(function(e) {
	  	
	  	e.preventDefault();
	  	
	  	var formData = JSON.parse(JSON.stringify($('.addProduct').serializeArray()));
	  	console.log(formData);
	  	$.ajax({
		    url: apiUrl+'/admin/addProduct',
		    type: 'POST',
		    data: formData,
		    success: function(resp) {
		      	if (resp.status) {
		      		showNotification('bg-black', 'Details has been added successfully');
		      		updateUITable(resp.data);
		      	} else {
			 		showNotification('bg-danger', 'Something went wrong!');
			 	}
		    }
		});
  	});

  	function updateUITable(data) {
  		if (data.length > 0) {
  			
  		}
  	}

  	$(document).on('click', '.deleteProduct', function function_name() {
		$('.deleteProductConfirm').attr('data-code', $(this).attr('data-code'))
		$('#deleteProductModal').modal('show');
	});

	$(document).on('click', '.deleteProductConfirm', function function_name() {
		
		var code = $(this).attr('data-code');
		var token = $(this).attr('data-csrf');
		if (code != '') {
			$.ajax({
				url: apiUrl+'/admin/deleteProduct',
				type: 'POST',
				data: {'code': code, "_token": token},
				success: function(resp) {
				 	if (resp.status == 'success') {
				 		showNotification('bg-black', 'Product has been deleted successfully');
			        	setTimeout(function() {
			        		window.location.reload(1);
			        	}, 4000);
				 	}else {
				 		showNotification('bg-danger', 'Something went wrong!');
				 	}
				}
		    });
		}
	});

}());